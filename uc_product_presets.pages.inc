<?php
/**
 * @file
 * TODO
 */

/**
 * Display a product preset edit form.
 */
function uc_product_preset_form($form_state, $node, $uc_product_preset) {
  $form = array();
  
  $form['#redirect'] = 'node/'. $uc_product_preset->nid;
  
  $form['prid'] = array(
    '#type' => 'value',
    '#value' => $uc_product_preset->prid,
  );
  
  $form['nid'] = array(
    '#type' => 'value',
    '#value' => $uc_product_preset->nid,
  );
  
  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $uc_product_preset->uid,
  );
   
  $form['data'] = array(
    '#type' => 'value',
    '#value' => serialize($uc_product_preset->data),
  );
  
  $form['timestamp'] = array(
    '#type' => 'value',
    '#value' => $uc_product_preset->timestamp,
  );
  
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('Please select a descriptive name for your preset. Presets without a name can\'t be published.'),
    '#required' => TRUE,
    '#size' => 60,
    '#maxlength' => 128,
    '#default_value' => isset($uc_product_preset->name) ? $uc_product_preset->name : '',
  );
  
  $form['published'] = array(
    '#type' => 'checkbox',
    '#title' => t('Published'),
    '#description' => t('Published presets can be accessed by other users. Unpublished presets will only be accessible through your account.'),
    '#default_value' => ($uc_product_preset->published == UC_PRODUCT_PRESETS_PUBLISHED) ? TRUE : FALSE,
  );
  
  $form['configuration'] = array(
    '#type' => 'markup',
    '#value' => theme('uc_product_preset_configuration', $uc_product_preset, $node),
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save changes'),
    '#suffix' => l(t('delete'), 'node/'. $uc_product_preset->nid .'/preset/'. $uc_product_preset->prid .'/delete'),
  );
  
  return $form;
}

/**
 * Save changes to product preset to the database.
 */
function uc_product_preset_form_submit($form, &$form_state) {
  // Create a product preset object.
  $uc_product_preset = new stdClass();
  $uc_product_preset->prid = $form_state['values']['prid'];
  $uc_product_preset->nid  = $form_state['values']['nid'];
  $uc_product_preset->uid  = $form_state['values']['uid'];
  $uc_product_preset->name = check_plain($form_state['values']['name']);
  $uc_product_preset->data = $form_state['values']['data'];    
  $uc_product_preset->timestamp = $form_state['values']['timestamp'];
  
  if ($form_state['values']['published'] == UC_PRODUCT_PRESETS_PUBLISHED) {
    $uc_product_preset->published = UC_PRODUCT_PRESETS_PUBLISHED;
  }
  else {
    $uc_product_preset->published = UC_PRODUCT_PRESETS_UNPUBLISHED;
  }
    
  // Save changes to database.
  uc_product_preset_save($uc_product_preset);
}

/**
 * Display a product preset delete confirmation form.
 */
function uc_product_preset_delete_form($form_state, $node, $uc_product_preset) {
  $form = array();

  $form['#redirect'] = 'node/'. $uc_product_preset->nid;

  $form['prid'] = array(
    '#type' => 'value',
    '#value' => $uc_product_preset->prid,
  );
  
  $form['configuration'] = array(
    '#type' => 'markup',
    '#value' => theme('uc_product_preset_configuration', $uc_product_preset, $node),
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Confirm'),
    '#suffix' => l(t('cancel'), 'node/'. $uc_product_preset->nid .'/preset/'. $uc_product_preset->prid),
  );
  
  return $form;
}

/**
 * Delete product preset from database.
 */
function uc_product_preset_delete_form_submit($form, &$form_state) {
  // Create a product preset object.
  $uc_product_preset = new stdClass();
  $uc_product_preset->prid = $form_state['values']['prid'];
  
  // Delete product preset from database.
  uc_product_preset_delete($uc_product_preset);
}

/**
 * Display all product presets for a specific user.
 */
function uc_product_presets_user_page($account = NULL) {
  // Load presets for this account.
  $uc_product_presets = _uc_product_preset_list(NULL, $account->uid);
  
  return theme('uc_product_presets_user', $uc_product_presets, $account);
}

/**
 * Display the user's product presets for a specific node.
 */
function uc_product_presets_node_page($node = NULL) {
  global $user;

  // Load presets for this node & account.
  $uc_product_presets = _uc_product_preset_list($node->nid, $user->uid);
  
  return theme('uc_product_presets_node', $uc_product_presets, $user, $node);
}